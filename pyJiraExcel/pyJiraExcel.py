from jira.client import JIRA
from dataclasses import dataclass
from openpyxl import Workbook, load_workbook
import datetime
from typing import List

@dataclass
class IssueData:
    key: str
    summary: str
    created: str
    originalEstimate: int
    fixVersion: str
    component: str
    epicLink: str
    #sprint???

@dataclass
class ChangelogItem:
    issueData: IssueData
    changeAuthor: str
    changeDate: str
    field: str
    fieldType: str
    fromm: str
    fromString: str
    to: str
    toString: str


# Issue fields
ISSUE_KEY = 0
SUMMARY = 1
CREATED = 2
ORIGINAL_ESTIMATE = 3
FIX_VERSION = 4
COMPONENT = 5
EPIC_LINK = 6
SPRINT = 7

CHANGE_AUTHOR = 4
CHANGE_DATE = 5
REMANING_ESTIMATE = 6

class JiraClient():
    def __init__(self,  username, password, url="https://jira.intelligentsystems.lan:8443/",):
        self.url = url
        self.username = username
        self.password = password
        self.dateLister = self.dateList(2020, 2, 4, 75)

    def stringToDate(self, _dateString):
        _datetime = self.stringToDateTime(_dateString)
        return datetime.date(year=_datetime.year, month=_datetime.month, day=_datetime.day)

    def stringToDateTime(self, _dateString):
        return datetime.datetime.strptime(_dateString, "%Y-%m-%dT%H:%M:%S.%f%z")

    def signin(self):
        options = {"server": self.url,  'verify': False}
        self.jira = JIRA(options, basic_auth=(self.username, self.password))

    def getIssuesFromJQL(self, jqlString, startAt=0, maxResults=1000, validate_query=True, fields=None, expand=None, json_result=None):
        resultList = self.jira.search_issues(jqlString, startAt=startAt, maxResults=maxResults, validate_query=validate_query, fields=fields, expand=expand, json_result=json_result)

        return resultList

    def getChangelog(self, issue):

        issue = self.jira.issue(issue, expand='changelog')
        changelog = issue.changelog
        return changelog

    def getChangelogItemsFromChangelog(self, changelog, issueData, itemField='timeestimate'):
        itemFields = []

        for history in changelog.histories:
            for item in history.items:
                if item.field == itemField:
                    logItem = ChangelogItem(issueData, history.author, history.created, item.field, item.fieldtype, item.fromString, item.fromString, item.to, item.toString)
                    itemFields.append(logItem)
                    fromString = item.fromString if item.fromString is not None else "None"
                    toString = item.toString if item.toString is not None else "None"
                    print('Date:' + history.created + '. ' + itemField + ' changed from:' + fromString + ' To:' + toString)

        return itemFields

    def getRemaningChangesFromResultList(self, resultList):
        allRemaningChanges = []
        datarows = []

        for nextIssue in resultList:
            changelog = self.getChangelog(nextIssue.key)

            currentIssueData = IssueData(key=nextIssue.key,
                                         summary=nextIssue.fields.summary,
                                         created=nextIssue.fields.created,
                                         originalEstimate=nextIssue.fields.aggregatetimeoriginalestimate,
                                         fixVersion=nextIssue.fields.fixVersions[0].name if len(nextIssue.fields.fixVersions) > 0 else "",
                                         component=nextIssue.fields.components[0].name if len(nextIssue.fields.components) > 0 else "",
                                         epicLink=nextIssue.fields.customfield_10081)

            itemFields = self.getChangelogItemsFromChangelog(changelog, currentIssueData, itemField='timeestimate')

            createdDateTime = datetime.datetime.strptime(nextIssue.fields.created, "%Y-%m-%dT%H:%M:%S.%f%z")
            createdDate = datetime.date(year=createdDateTime.year, month=createdDateTime.month, day=createdDateTime.day)

            itemFields.reverse()

            originalEstimateHours = float(int(currentIssueData.originalEstimate)/60/60) if currentIssueData.originalEstimate is not None else 0

            for date in self.dateLister:
                # date = datetime.datetime(
                #             year=date.year,
                #             month=date.month,
                #             day=date.day,
                #         )

                row = None

                # Date created. Remaning set to original estimate
                if date == createdDate:
                    print(currentIssueData.originalEstimate)
                    row = [date, currentIssueData.key, currentIssueData.summary, currentIssueData.created[:10],
                           originalEstimateHours,
                           currentIssueData.fixVersion, currentIssueData.component, currentIssueData.epicLink,
                           nextIssue.fields.reporter.name,
                           currentIssueData.created[:10], originalEstimateHours]
                # Issue is closed and not updated after "date". Assuming remaning i "0"
                elif date > self.stringToDate(nextIssue.fields.updated) and nextIssue.fields.status.name == "Closed":
                    row = [date, currentIssueData.key, currentIssueData.summary, currentIssueData.created[:10],
                           originalEstimateHours,
                           currentIssueData.fixVersion, currentIssueData.component, currentIssueData.epicLink,
                           nextIssue.fields.reporter.name,
                           currentIssueData.created[:10], 0]
                elif date > createdDate and len(itemFields) > 0:
                    for change in itemFields:
                        if date >= self.stringToDate(change.changeDate):
                            # Found changed remaning estimate creating row
                            row = [date, currentIssueData.key, currentIssueData.summary, currentIssueData.created[:10],
                                   originalEstimateHours,
                                   currentIssueData.fixVersion, currentIssueData.component, currentIssueData.epicLink,
                                   change.changeAuthor.name,
                                   change.changeDate[:10], float(int(change.to)/60/60)]
                            break
                        else:
                            # Did not find changed remaning estimate creating row with original estimate
                            row = [date, currentIssueData.key, currentIssueData.summary, currentIssueData.created[:10],
                                   originalEstimateHours,
                                   currentIssueData.fixVersion, currentIssueData.component, currentIssueData.epicLink,
                                   nextIssue.fields.reporter.name,
                                   currentIssueData.created[:10], originalEstimateHours]
                # No timeestimate changed in changelog
                elif date > createdDate:
                    row = [date, currentIssueData.key, currentIssueData.summary, currentIssueData.created[:10],
                           originalEstimateHours,
                           currentIssueData.fixVersion, currentIssueData.component, currentIssueData.epicLink,
                           nextIssue.fields.reporter.name,
                           currentIssueData.created[:10], originalEstimateHours]


                else:
                    print("Next")

                if row is not None:
                    datarows.append(row)

                    if nextIssue.fields.status.name == "Closed" and row[10] > 0:
                        print("Remaning not set in changelog")



            allRemaningChanges.extend(itemFields)

        return allRemaningChanges, datarows


    def dateList(self, startYear, startDay, startMonth, numOfDays):
        base = datetime.date(year=startYear, day=startDay, month=startMonth)
        date_list = [base + datetime.timedelta(days=x) for x in range(numOfDays)]

        return date_list

                    ##https: // github.com / redtoad / jira - history / tree / master / jira_history


class ExcelCon():
    def __init__(self, filename="hello_world.xlsx"):
        self.filename = filename
        self.openWorkbook()
        self.sheet = self.workbook.active

    def openWorkbook(self):
        self.workbook = load_workbook(filename=self.filename)

    def selectSheetByName(self, sheetname):
        self.sheet = self.workbook[sheetname]

    def getSheetByName(self, sheetName):
        self.sheet = self.workbook.get_sheet_by_name(sheetName)


    def getSheetByNumber(self, sheetNumber=0):
        sheetNames = self.workbook.get_sheet_names()
        self.workbook.getSheetByName(sheetNames[sheetNumber])

    def writeToCell(self):
        self.sheet["A1"] = "hello"
        self.sheet["B1"] = "world!"

    def writeRemaningEstimatestoSheet(self, myIssues):

        self.sheet.append(["Key", "Summary", "Created",
              "originalEstimate", "fixVersion", "component", "epicLink", "changeAuthor", "changeDate", "remaningEstimate"])

        for issue in myIssues:
            issue.issueData.key
            row = [issue.issueData.key, issue.issueData.summary, issue.issueData.created[:10], issue.issueData.originalEstimate,
                   issue.issueData.fixVersion, issue.issueData.component, issue.issueData.epicLink, issue.changeAuthor.name,
                   issue.changeDate[:10], issue.toString]

            self.sheet.append(row)

    def writeDatarows(self, datarows):
        self.sheet.append(["Date", "Key", "Summary", "Created",
                           "originalEstimate", "fixVersion", "component", "epicLink",
                           "changeAuthor", "changeDate", "remaningEstimate"])

        for row in datarows:
            self.sheet.append(row)

    def saveWorkbok(self):
        self.workbook.save(self.filename)


# support for multipel components and fixversion
# multiple changes per date
#
