from pyJiraExcel import pyJiraExcel

excel = pyJiraExcel.ExcelCon(filename='test.xlsx')

jiraClient = pyJiraExcel.JiraClient('ppo', 'pdSA120!')

dateList = jiraClient.dateList(2020, 1, 3, 60)

jiraClient.signin()

resultList = jiraClient.getIssuesFromJQL('project = BPV and issuetype != Epic', maxResults=1000)
#resultList = jiraClient.getIssuesFromJQL('project = BPV and issuetype in standardIssueTypes() and Sprint = 86', maxResults=1000)
#resultList = jiraClient.getIssuesFromJQL('key = BPV-46', maxResults=1000)
#resultList = jiraClient.getIssuesFromJQL('key = BPV-24', maxResults=1000)

myIssues, datarows = jiraClient.getRemaningChangesFromResultList(resultList)

excel.selectSheetByName("Datarows")

#excel.writeRemaningEstimatestoSheet(myIssues)

excel.writeDatarows(datarows)

excel.saveWorkbok()